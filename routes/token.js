const request = require('request');
var constants = require("./constants");

getToken = async (payid, callbackfunc) => {
    const url = 'https://api.test.paysafe.com/paymenthub/v1/customers';
    var values = {
        "merchantRefNum": "MerRef1234567",
        "paymentTypes": [
            "CARD"
        ]
    }
    const options = {
        url: 'https://api.test.paysafe.com/paymenthub/v1/customers/' + payid + '/singleusecustomertokens',
        headers: constants.headers,
        body: JSON.stringify(values),
        method: 'POST'
    };
    function callback(error, response, body) {
        return callbackfunc(JSON.parse(body).singleUseCustomerToken);
    }

    request(options, callback);
};

module.exports.getToken = getToken;