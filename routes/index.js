const request = require('request');
var constants = require("./constants");

getId = async (req, callbackfunc) => {
    const url = 'https://api.test.paysafe.com/paymenthub/v1/customers';
    var values = {
        merchantCustomerId: req.email + req.firstName + req.phone + "8831809809102904990815",
        firstName: req.firstName,
        email: req.email,
        phone: req.phone,
    }

    const options = {
        url: url,
        headers: constants.headers,
        body: JSON.stringify(values),
        method: 'POST'
    };
    function callback(error, response, body) {
        return callbackfunc(JSON.parse(body).id);
    }

    request(options, callback);
};

module.exports.getId = getId;