# ROIIM-PAYSAFE-ASSIGNMENT

> Problem Statement
>
> Build a simple integration as an e-commerce merchant would do with Paysafe Checkout.
>
> - [x] Integrate with the only Card as the payment method
>
> - [x] Implement the save card flow. Pre-filled form to checkout with test card.

##### [ Live Preview](https://roiim-paysafe-api.herokuapp.com/)

#### Technologies used

- **HTML/BootStrap/AJaX (Frontend)**

- **MongoDB/NodeJS(Backend)**

- **PaySafe Checkout API**

  

### Steps to locally run the Project

```bash
git clone <repo>
npm install
node server.js
```



